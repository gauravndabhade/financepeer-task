# Financepeer-Assignment


## Initialization

Create virtual environment and install requirements

```sh
$ python3.6 -m venv env
$ source env/bin/activate
(env) $
(env) $ pip install -r requirements.txt

```
Make migrations, migrate and create superuser

```sh
(env) $ python manage.py makemigrations
(env) $ python manage.py migrate
(env) $ python manage.py createsuperuser
```

Webpack
```sh
$ npm install
$ npm run prod
$ npm run dev # development
```

Start application
```sh
python manage.py runserver 0.0.0.0:8000
```

App will start in brower in [`http://0.0.0.0:8000`](http://127.0.0.1:8000)

## Results

![Landing Page](/screenshots/fp1.PNG)
![Landing Page 2](/screenshots/fp3.PNG)
![Home Page](/screenshots/fp2.PNG)


## Thanks 

Using django-react-redux-blueprint for this applications. 
For detailed explanation refer [this](https://medium.com/@maikluchtmeyer) blog post by `Maik Luchtmeyer`
