import { CONSTANTS } from '../constants'

const initialState = {
  user: null,
  errors: {},
};

export default function Reducer(state = initialState, action) {
  switch (action.type) {
  case CONSTANTS.AUTH.LOGIN_SUCCESSFUL:
    return Object.assign({}, state, {
      user: action.data,
      errors: null,
    });

  case CONSTANTS.AUTH.LOGIN_ERROR:
    return Object.assign({}, state, {
      errors: action.data,
      user: null,
    });

  case CONSTANTS.AUTH.SIGNUP_SUCCESSFUL:
    return Object.assign({}, state, {
      user: action.data,
      errors: null,
    });

  case CONSTANTS.AUTH.SIGNUP_ERROR:
    return Object.assign({}, state, {
      errors: action.data,
      user: null,
    });

  case CONSTANTS.AUTH.LOGOUT_SUCCESSFUL:
    return Object.assign({}, state, {
      message: action.data
    })

  case CONSTANTS.FILE_UPLOAD_SUCCESSFUL:
    return Object.assign({}, state, {
      data: action.data,
      error: null
    });

  case CONSTANTS.FILE_UPLOAD_FAILED:
    return Object.assign({}, state, {
      error: action.data,
      data: null
    });

  case CONSTANTS.GET_POSTS_SUCCESSFUL:
    return Object.assign({}, state, {
      data: action.data,
      error: null
    });

  case CONSTANTS.GET_POSTS_FAILED:
    return Object.assign({}, state, {
      error: action.data,
      data: null
    })
    
  default:
    return state;
  }
}
