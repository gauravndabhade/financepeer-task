import jQuery from 'jquery';
import { CONSTANTS } from '../constants'
// import history from '../history';

export function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    const cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      const cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

export function getToken() {
  let user = JSON.parse(localStorage.getItem(CONSTANTS.LOCAL_STORAGE.USER_SESSION))
  if (user && user.token != null && user.token != undefined) {
    return user.token;
  }
}

export function isAuthenticated() {
  let data = JSON.parse(localStorage.getItem(CONSTANTS.LOCAL_STORAGE.USER_SESSION))
  return (data && data.token != null && data.token != undefined) ? true : false;
}

export function login(username, password) {
  return (dispatch, getState) => {
    const csrftoken = getCookie('csrftoken');
    const authParams = JSON.stringify({ email: username, password: password });
    return fetch('/user/login/', {
      method: 'POST',
      body: authParams,
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': csrftoken,
      },
    })
      .then((response) => {
        return response.json().then((data) => {
          return {
            status: response.status,
            data,
          };
        },
        );
      })
      .then((response) => {
        if (response.status !== 200) {
          dispatch({ type: CONSTANTS.AUTH.LOGIN_ERROR, data: response.status });
        }
        if (response.status === 202) {
          // Update to localstorage
          localStorage.setItem(CONSTANTS.LOCAL_STORAGE.USER_SESSION, JSON.stringify(response.data));
          dispatch({ type: CONSTANTS.AUTH.LOGIN_SUCCESSFUL, data: response.data });
        }
      });
  };
}


export function signup(name, email, password) {
  return (dispatch, getState) => {
    const csrftoken = getCookie('csrftoken');
    const authParams = JSON.stringify({
      name: name,
      email: email,
      password: password
    });

    return fetch('/user/signup/', {
      method: 'POST',
      body: authParams,
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': csrftoken,
      },
    })
      .then((response) => {
        return response.json().then((data) => {
          return {
            status: response.status,
            data,
          };
        },
        );
      })
      .then((response) => {
        if (response.status !== 200) {
          dispatch({ type: CONSTANTS.AUTH.SIGNUP_ERROR, data: response.status });
        }
        if (response.status === 201) {
          // Update to localstorage
          localStorage.setItem(CONSTANTS.LOCAL_STORAGE.USER_SESSION, JSON.stringify(response.data));
          dispatch({ type: CONSTANTS.AUTH.SIGNUP_SUCCESSFUL, data: response.data });
        }
      });
  };
}


export function logout() {
  return (dispatch, getState) => {
    return fetch('/user/logout/', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "Token " + getToken()
      },
    })
      .then((response) => {
        return response.json().then((data) => {
          return {
            status: response.status,
            data,
          };
        },
        );
      })
      .then((response) => {
        if (response.status !== 200) {
          dispatch({ type: CONSTANTS.AUTH.LOGOUT_ERROR, data: response.status });
        }
        if (response.status === 200) {
          // Update to localstorage
          localStorage.removeItem(CONSTANTS.LOCAL_STORAGE.USER_SESSION);
          dispatch({ type: CONSTANTS.AUTH.LOGOUT_SUCCESSFUL, data: response.data });
        }
      });
  };
}

export function uploadData(file) {
  console.log(file);
  const formData = new FormData()
  formData.append('myfile', file)
  for (var key of formData.entries()) {
    console.log(key[0] + ', ' + key[1]);
  }
  return (dispatch, getState) => {
    return fetch('/user/uploadJson/', {
      method: "POST",
      headers: {
        // 'Content-Type': 'multipart/form-data',
        'X-CSRFToken': getCookie('csrftoken'),
        'Authorization': "Token " + getToken()
      },
      'body': formData
    })
      .then((response) => {
        return response.json().then((data) => {
          return {
            status: response.status,
            data,
          };
        },
        );
      })
      .then((response) => {
        if (response.status !== 200) {
          console.log(response)
          dispatch({ type: CONSTANTS.FILE_UPLOAD_FAILED, data: response.status });
        }
        if (response.status === 200) {
          dispatch({ type: CONSTANTS.FILE_UPLOAD_SUCCESSFUL, data: response.data });
        }
      });
  }
}

export function getPosts() {
  return (dispatch, getState) => {
    return fetch('/user/posts/', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "Token " + getToken()
      },
    })
      .then((response) => {
        return response.json().then((data) => {
          return {
            status: response.status,
            data,
          };
        },
        );
      })
      .then((response) => {
        if (response.status !== 200) {
          dispatch({ type: CONSTANTS.GET_POSTS_FAILED, data: response.status });
        }
        if (response.status === 200) {
          dispatch({ type: CONSTANTS.GET_POSTS_SUCCESSFUL, data: response.data });
        }
      });
  };
}
