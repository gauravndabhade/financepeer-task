import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from './components/Menu.jsx';
import store from './store/store';


ReactDOM.render((
  <Provider store={store}>
    <div>
      <Menu />
    </div>
  </Provider>
), document.getElementById('react-app'));
