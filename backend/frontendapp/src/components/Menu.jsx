import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap'
import { BrowserRouter as Router, Redirect, Link, Route } from 'react-router-dom'
import LandingPage from './LandingPage/LandingPage.jsx';
import { CONSTANTS } from '../constants'
import { connect } from 'react-redux';
import { logout, isAuthenticated } from '../actions/auth';

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: JSON.parse(localStorage.getItem(CONSTANTS.LOCAL_STORAGE.USER_SESSION)),
            login_page: true,
            data: null
        }
    }

    componentDidMount() {
        // console.log("componentDidMount()")
        this.setState({
            user: JSON.parse(localStorage.getItem(CONSTANTS.LOCAL_STORAGE.USER_SESSION)),
        })
    }

    static getDerivedStateFromProps(props, current_state) {
        console.log(">>>")
        console.log(props)
        if (current_state.posts !== props.data) {
            return { data: props.data }
        }
        return null
    }


    render() {
        return (

            <Router>
                <Navbar bg="light" expand="lg">
                    <Navbar.Brand>FinancePeer</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link><Link to={{
                                pathname: "/",
                                state: {
                                    login_page: true
                                }
                            }}
                            >Home</Link></Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                    <Navbar.Collapse className="justify-content-end">
                        {isAuthenticated() ?

                            <NavDropdown title={this.state.user.email} id="basic-nav-dropdown">
                                <NavDropdown.Item onClick={() => { this.props.logout(); console.log("Click Logout") }}>Logout</NavDropdown.Item>
                            </NavDropdown>
                            :
                            <Nav className="me-auto">
                                {this.state.login_page ?
                                    <Nav.Link onClick={() => { this.setState({ login_page: false }) }}><Link to={{
                                        pathname: "/",
                                        state: {
                                            login_page: false
                                        }
                                    }}
                                    >Create New Account</Link></Nav.Link>
                                    :
                                    <Nav.Link onClick={() => { this.setState({ login_page: true }) }}><Link to={{
                                        pathname: "/",
                                        state: {
                                            login_page: true
                                        }
                                    }}
                                    >Login</Link></Nav.Link>
                                }
                            </Nav>
                        }
                    </Navbar.Collapse>
                </Navbar >
                <Route path="/" exact component={LandingPage} />
            </Router>
        )
    }
}

const mapStateToProps = (props) => {
    return {
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => dispatch(logout())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Menu)
