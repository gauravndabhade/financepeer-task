import React, { Component } from 'react'
import { Row, Col, Container } from 'react-bootstrap';
import LeftSide from './LeftSide.jsx';
import Signup from './Signup.jsx';
import Login from './login.jsx';
import { CONSTANTS } from '../../constants';
import { isAuthenticated } from '../../actions/auth';
import Main from './Main.jsx';


export class LandingPage extends Component {
    constructor(props) {
        super(props);
        this.props.location.state = {
            login_page: true
        }
        this.state = {
            user: JSON.parse(localStorage.getItem(CONSTANTS.LOCAL_STORAGE.USER_SESSION)),
        }
    }

    componentDidMount() {
        console.log("componentDidMount()")
        this.setState({
            user: JSON.parse(localStorage.getItem(CONSTANTS.LOCAL_STORAGE.USER_SESSION)),
        })
    }

    render() {
        return (
            <>
                {isAuthenticated() ?
                    <>
                        <Main />
                    </>
                    :
                    <Container>
                        <Row>
                            <Col className="d-none d-lg-block"><LeftSide /></Col>
                            <Col>
                                {
                                    this.props.location.state.login_page ?
                                        <Login />
                                        :
                                        <Signup />
                                }
                            </Col>
                        </Row>
                    </Container>
                }
            </>
        )
    }
}

export default LandingPage