import React, { Component } from 'react'
import {Image} from 'react-bootstrap'; 

export class LeftSide extends Component {
    render() {
        return (
            <div>
                <Image src="./static/images/Financepeer-EWIER.jpg" thumbnail style={{border: "none"}}></Image>
            </div>
        )
    }
}

export default LeftSide
