import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Container from 'react-bootstrap/lib/Container';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Form from 'react-bootstrap/lib/Form';
import Button from 'react-bootstrap/lib/Button';
import { signup } from '../../actions/auth';
import {Image} from 'react-bootstrap'; 
import { Link } from 'react-router-dom';


class Signup extends Component {

  
  constructor(props) {
    super(props);
    this.state = {
      data: null
    }
  }

  static getDerivedStateFromProps(props, current_state) {
    if (current_state.posts !== props.data) {
      return { data: props.data }
    }
    return null
  }

  static propTypes = {
    _signup: PropTypes.func.isRequired,
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { name, email, password } = this.state;
    const { _signup } = this.props;
    _signup(name, email, password);
  }


  render() {
    return (
      <Container fluid="True">
        <Row className="align-items-center">
          <Col className="text-center">
            <Image src="./static/images/logo.jpg" thumbnail style={{ border: "none" }}></Image>
            
            <Form onSubmit={this.onSubmit}>
              <Form.Group controlId="formBasicName">
                <Form.Control
                  type="name"
                  placeholder="Full Nane"
                  onChange={e => this.setState({ name: e.target.value })}
                />
              </Form.Group>
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  type="email"
                  placeholder="Email"
                  onChange={e => this.setState({ email: e.target.value })}
                />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Control
                  type="password"
                  placeholder="Password"
                  onChange={e => this.setState({ password: e.target.value })}
                />
              </Form.Group>

              <Form.Group controlId="formBasicPassword2">
                <Form.Control
                  type="password"
                  placeholder="Confirm Password"
                  onChange={e => this.setState({ password2: e.target.value })}
                />
              </Form.Group>
              <Button variant="primary mr-1" type="submit">
                Submit
              </Button>
              <Button type="reset" className="btn btn-primary">Reset</Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  _signup: (name, email, password) => {
    dispatch(signup(name, email, password));
  },
});

const mapStateToProps = (props) => {
  return {
      data: props.data
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
