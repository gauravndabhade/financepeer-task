import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Container from 'react-bootstrap/lib/Container';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Form from 'react-bootstrap/lib/Form';
import Button from 'react-bootstrap/lib/Button';
import { login } from '../../actions/auth';
import { Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class Login extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      data: null
    }
  }

  static propTypes = {
    _login: PropTypes.func.isRequired,
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { username, password } = this.state;
    const { _login } = this.props;
    _login(username, password);
  }

  static getDerivedStateFromProps(props, current_state) {
    if (current_state.posts !== props.data) {
      return { data: props.data }
    }
    return null
  }

  render() {
    return (
      <Container fluid="True">
        <Row className="align-items-center">
          <Col className="text-center">
            <Image src="./static/images/logo.jpg" thumbnail style={{ border: "none" }}></Image>

            <Form onSubmit={this.onSubmit}>
              <Form.Group controlId="formBasicName">
                <Form.Control
                  type="name"
                  placeholder="Userame"
                  onChange={e => this.setState({ username: e.target.value })}
                />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Control
                  type="password"
                  placeholder="Password"
                  onChange={e => this.setState({ password: e.target.value })}
                />
              </Form.Group>
              <Button variant="primary mr-1" type="submit">
                Submit
              </Button>
              <Button type="reset" className="btn btn-primary">Reset</Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (props) => {
  return {
    data: props.data
  }
}

const mapDispatchToProps = dispatch => ({
  _login: (username, password) => {
    console.log(dispatch(login(username, password)));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
