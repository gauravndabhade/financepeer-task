import React, { Component } from 'react'
import { Row, Col, Container, Form, Button, Table } from 'react-bootstrap';
import { uploadData, getPosts } from '../../actions/auth';
import { connect } from 'react-redux';

export class Main extends Component {

    constructor(props) {
        super(props)
        this.state = {
            posts: null
        }
    }

    componentDidMount() {
        console.log("MainToolBar componentDidMount()");
        this.props.getPosts();
    }

    static getDerivedStateFromProps(props, current_state) {
        if (current_state.posts !== props.data) {
            return { posts: props.data }
        }
        return null
    }

    handleFileSubmit = (event) => {
        event.persist();
        const files = event.target.files
        this.props.uploadData(files[0]);
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col className="mt-5">
                        <input type="file" class="custom-file-input" id="customFile" onChange={(event) => this.handleFileSubmit(event)} ></input>
                        <label class="custom-file-label" for="customFile">Choose (.json) file</label>
                    </Col>
                </Row>
                <Row></Row>
                <Row>
                    <Table className="mt-5" striped bordered hover>
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.posts ? this.state.posts.map((item) => {
                                return <tr>
                                    <td>{item.title}</td>
                                    <td>{item.description}</td>
                                </tr>
                            }) :
                                <>Hello</>
                            }
                        </tbody>
                    </Table>
                </Row>
            </Container>
        )
    }
}

const mapStateToProps = (props) => {
    return {
        data: props.data
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        uploadData: (formData) => dispatch(uploadData(formData)),
        getPosts: () => dispatch(getPosts())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Main);