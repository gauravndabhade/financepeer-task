from django.contrib.auth.models import User
from rest_framework.decorators import api_view, permission_classes
from django.core.exceptions import ValidationError
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token
from django.contrib.auth.hashers import check_password
from django.contrib.auth import login, logout
import json
from django.db import connection
from django.db import IntegrityError

from backendapp.serializer import UserSerializer, RegistrationSerializer, DataUploadSummarySerializer
from backendapp.models import Users, Post
from backendapp.models import DataUploadSummary, Post
from backendapp.serializer import PostSerializer


@api_view(["POST"])
@permission_classes([AllowAny])
def register_user(request):
    try:
        serializer = RegistrationSerializer(data=request.data)
        if serializer.is_valid():
            account = serializer.save()
            account.is_active = True
            account.save()
            token = Token.objects.get_or_create(user=account)[0].key

            context = {
                'message' : "User registered successfully",
                'email' : account.email,
                'token' : token
            }
            return Response(context, status=status.HTTP_201_CREATED)
        else:
            data = serializer.errors
            return Response(data, status=status.HTTP_409_CONFLICT)

    except IntegrityError as e:
        return Response({"message": f'User already exists'}, status=status.HTTP_400_BAD_REQUEST)

    except KeyError as e:
        return Response({"message": f'Field {str(e)} missing'}, status=status.HTTP_400_BAD_REQUEST)



@api_view(["POST"])
@permission_classes([AllowAny])
def login_user(request):
        data = {}
        reqBody = json.loads(request.body)
        email1 = reqBody['email']
        password = reqBody['password']
        if (Users.objects.filter(email=email1).exists()):
            Account = Users.objects.get(email=email1)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)

        token = Token.objects.get_or_create(user=Account)[0].key

        if Account.is_active:
            login(request, Account)
            context = {
                "message" : "User logged in successfully",
                "email" : Account.email,
                "token" : token
            }
            return Response(context, status=status.HTTP_202_ACCEPTED)
        else:
            raise Response({"message": f'{Account.email} does not active'}, status=status.HTTP_406_NOT_ACCEPTABLE)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def user_logout(request):
    request.user.auth_token.delete()
    logout(request)
    return Response({"message":'User Logged out successfully'}, status=status.HTTP_200_OK)


    
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def upload_file(request): 
    file = request.FILES['myfile']
    if(file):
        try:
            # Upload Data Upload Summary
            kwargs = {
                'filename': file.name, 
                'path': file.name, 
                'user': request.user
            }
            d_summary = DataUploadSummary(**kwargs)
            d_summary.save()

            # Upload Posts
            posts = json.loads(file.read().decode('utf-8')) 
            for post in posts:
                print(post)
                user = Users.objects.get(id=post['userId'])
                if (user):
                    try:
                        p = Post(user=user, post_id=post["id"], title=post["title"], description=post["body"])
                        p.save()
                    except Exception as e:
                        print(f"Exception: {e}")
                else:
                    return Response({"message": "User does not exists"})

            return Response({'message': "Successful"})
        except Exception as e:
            print(e)
            return Response({"message": "Something went wrong"})


@api_view(['GET'])
@permission_classes([AllowAny])
def all_posts(request):
    posts = Post.objects.all()
    if request.method == 'GET':
        serializer = PostSerializer(posts, many=True)
        return Response(serializer.data)

@api_view(['GET'])
@permission_classes([AllowAny])
def data_upload_summary(request):
    data = DataUploadSummary.objects.all()
    if request.method == 'GET':
        serializer = DataUploadSummarySerializer(data, many=True)
        return Response(serializer.data)