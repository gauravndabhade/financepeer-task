from django.urls import path
from . import views

urlpatterns = [    
    path('login/',views.login_user),
    path('logout/',views.user_logout),
    path('signup/',views.register_user),
    path('uploadJson/', views.upload_file),
    path('posts/', views.all_posts),
    path('data_upload_summary/', views.data_upload_summary),
]
