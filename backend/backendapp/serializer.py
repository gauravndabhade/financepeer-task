from rest_framework import serializers
from backendapp.models import Users, Post, DataUploadSummary


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Users
        fields = '__all__'


class RegistrationSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Users
        fields = [
            "email",
            "name",
            "password",
        ]

        extra_kwargs = {"password": {"write_only": True}}


    def create(self, validated_data):
        print('>>',validated_data)
        account = Users(
            email=validated_data['email'], name=validated_data['name']
        )
        password = self.validated_data["password"]
        account.set_password(password)
        account.save()
        return account
                                    
class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('user', 'title', 'description')


class DataUploadSummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = DataUploadSummary
        fields = ('user', 'filename', 'created_at')
