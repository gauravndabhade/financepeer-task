from django.contrib import admin
from backendapp.models import Users, Post, DataUploadSummary

class UsersAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email')

class PostAdmin(admin.ModelAdmin):
    list_display = ('post_id', 'user', 'title', 'description')

class DataUploadSummaryAdmin(admin.ModelAdmin):
    list_display = ('filename', 'created_at')

admin.site.register(Users, UsersAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(DataUploadSummary, DataUploadSummaryAdmin)
