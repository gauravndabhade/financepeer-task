from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.db import models

class MyAccountManager(BaseUserManager):
    def create_user(self, email, fullname=None, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            name=fullname,
        )

        user.set_password(password)
        user.is_admin = True
        user.is_active=True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def create_superuser(self, name, email, password):
        user = self.create_user(
            name=name,
            email=self.normalize_email(email),
            password=password,
        )
        user.is_admin = True
        user.is_active=True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

class Users(AbstractBaseUser):
    email = models.EmailField(verbose_name="email", max_length=60, unique=True, blank=True, null=True, default=None)
    name = models.CharField(max_length=30, blank=True, null=True)
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active=models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'

    objects = MyAccountManager()

    class Meta:
        db_table = "users"

    def __str__(self):
        return str(self.email)


    def has_perm(self, perm, obj=None): return self.is_superuser

    def has_module_perms(self, app_label): return self.is_superuser


class DataUploadSummary(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    filename = models.CharField(max_length=50)
    path = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "data_upload_summary"

    def __str__(self):
        return f"{self.filename} uploaded by {self.user.name}"

class Post(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    post_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = "posts"

    def __str__(self):
        return f"Post title: {self.title}"

