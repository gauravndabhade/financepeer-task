from django.conf.urls import include
from django.urls import path
from django.contrib import admin
from frontendapp import urls as frontendapp_urls
from backendapp import urls as backendapp_urls

urlpatterns = [
    path('', include(frontendapp_urls)),
    path('user/', include(backendapp_urls)),
    path('admin/', admin.site.urls),
]
